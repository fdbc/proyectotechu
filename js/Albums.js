// this file contains the data we need for the gallery
// The main object, "gallery" contains an array of album
// each album contains an array of photos 
// (plus a name and a thumbnail image)
// The photos contain an image src and some metadata

var gallery = {
	albums : [
		{
			name : "Exteriores",
			thumbnail : "../imagenes/jardin03.jpg",
			photos : [
				{
					src : "../imagenes/jardin01.jpg",
					title : "Parking",
					description : "Vista desde el parking."
				},
				{
					src : "../imagenes/jardin02.jpg",
					title : "Ventanal",
					description : "Acceso a salon privado."
				},
				{
					src : "../imagenes/jardin03.jpg",
					title : "Olivos",
					description : "Como parte de nuestra cultura. "
				},
				{
					src : "../imagenes/jardin04.jpg",
					title : "Jardines",
					description : "Presentes en todas nuestras instalaciones."
				},
                {
					src : "../imagenes/jardin05.jpg",
					title : "Flores",
					description : "Un entorno de bienvenida."
				},
				{
					src : "../imagenes/jardin06.jpg",
					title : "Luz",
					description : "Naturaleza en todos los Salones."
				},
                {
					src : "../imagenes/jardin07.jpg",
					title : "Agua",
					description : "Varias fuentes acompañan a nuestros clientes y amigos."
				},
				{
					src : "../imagenes/jardin08.jpg",
					title : "Arquitectura",
					description : "Un edificio ideado para ser un restaurante."
				}
			]
		},
		{
			name : "La terraza",
			thumbnail : "../imagenes/terra_04.jpg",
			photos : [
				{
					src : "../imagenes/terra_01.jpg",
					title : "Abierta",
					description : "Techo retractil cuando el tiempo acompaña."
				},
				{
					src : "../imagenes/terra_02.jpg",
					title : "Acceso",
					description : "Directo desde los jardines"
				},
				{
					src : "../imagenes/terra_03.jpg",
					title : "Agua",
					description : "Tres fuentes decoran el entorno"
				},
				{
					src : "../imagenes/terra_04.jpg",
					title : "Amigos",
					description : "Perfecto para celebrar con los que más queremos."
				},
                {
					src : "../imagenes/terra_05.jpg",
					title : "Tranquila",
					description : "¿Que más pedir para disfrutar de una excelente comida?."
				}
			]
		},
		{
			name : "Salones",
			thumbnail : "../imagenes/salon_01.jpg",
			photos : [
				{
					src : "../imagenes/salon_01.jpg",
					title : "Redondas",
					description : "Amplias mesas para no perderse conversaciones."
				},
				{
					src : "../imagenes/salon_02.jpg",
					title : "comodo",
					description : " instalaciones pensadas para nuestros clientes."
				},
				{
					src : "../imagenes/salon_03.jpg",
					title : "bovedas",
					description : "Grandes bovedas para amplios espacios."
				}
			]
		},
        {
			name : "La barra",
			thumbnail : "../imagenes/barra_04.jpg",
			photos : [
				{
					src : "../imagenes/barra_01.jpg",
					title : "Amplio",
					description : "Excelencia en nuestras instalaciones."
				},
				{
					src : "../imagenes/barra_02.jpg",
					title : "Bodegón",
					description : "El anticipo a una excelente comida."
				},
				{
					src : "../imagenes/barra_03.jpg",
					title : "Entrada",
					description : "Acceso principal al Brasero de Don Pedro."
				},
                {
					src : "../imagenes/barra_04.jpg",
					title : "Barra",
					description : "Un amplio entorno donde esperar con un aperitivo."
				},
				{
					src : "../imagenes/barra_05.jpg",
					title : "Tragaluz",
					description : "Luz natural en los accesos a los salones."
				},
				{
					src : "../imagenes/barra_06.jpg",
					title : "Frescor",
					description : "Grifos de cerveza refrigerados"
				},
				{
					src : "../imagenes/barra_07.jpg",
					title : "Bodega",
					description : "Disfrute de nuestros vinos con la carne o pescado elegido."
				},
				{
					src : "../imagenes/barra_08.jpg",
					title : "Ceramica",
					description : "Vasijas como parte de nuestra cultura."
				}
			]
		},
        {
			name : "Salones privados",
			thumbnail : "../imagenes/priva_02.jpg",
			photos : [
				{
					src : "../imagenes/priva_01.jpg",
					title : "Proyecciones",
					description : "Disponemos de proyector para sus presentaciones de empresa."
				},
				{
					src : "../imagenes/priva_02.jpg",
					title : "Comuniones",
					description : "Perfectos para celebrar eventos familiares con los que mas quiere."
				},
				{
					src : "../imagenes/priva_03.jpg",
					title : "Reservados",
					description : "Planifique con tiempo ese momento especial."
				},
                {
					src : "../imagenes/priva_04.jpg",
					title : "Privacidad",
					description : "Disfrute, en privado, de ese evento ideado con tanto detalle."
				},
				{
					src : "../imagenes/priva_05.jpg",
					title : "Grupos",
					description : "Distintas opciones en función del número de comensales."
				}
			]
		}

	]
};